package com.example.imagein.ui.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.media.MediaPlayer
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.imagein.ui.viewModel.ImageInViewModel
import com.example.imagein.R
import com.example.imagein.data.models.Photograph
import com.example.imagein.ui.views.DetailFragmentDirections

/*
Context -> Used to load the images and to use the vibration effect.
ID -> Used to update the detail's favorites icon.
View model -> Used to set the 'comes from detail' variable as true, so the enter animation of the
              detail fragment doesn't play when the favorites icon gets updated.
*/
class DetailAdapter(private val photographs: List<Photograph>, private val context: Context,
                    private val id: Int, private var viewModel: ImageInViewModel) :
    RecyclerView.Adapter<DetailAdapter.DetailViewHolder>() {
    //ADAPTER METHODS
    /**
     * This method is used to create a View Holder and to set up it's view.
     * @param parent Adapter's parent (used to get context)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.detail_recyclerview_item,
            parent, false)

        return DetailViewHolder(view)
    }

    /**
     * This method is used to set up the data of each item.
     * @param holder View Holder
     * @param position Current item
     */
    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        holder.bindData(photographs[position], context)

        //ANIMATION
        holder.itemView.animation = AnimationUtils.loadAnimation(holder.itemView.context,
            R.anim.list_animation
        )

        //FAVORITE
        val favoriteIcon: ImageView = holder.itemView.findViewById(R.id.favorites)

        //ON CLICK
        //item
        holder.itemView.setOnClickListener {
            val action = DetailFragmentDirections.actionDetailSelf(photographs[position].id)
            Navigation.findNavController(holder.itemView).navigate(action)
        }

        //favorites
        favoriteIcon.setOnClickListener {
            //ANIMATION
            YoYo.with(Techniques.Tada).duration(300).playOn(favoriteIcon)

            //SOUND (See Settings)
            if (viewModel.soundOn) {
                val mp = MediaPlayer.create(context, R.raw.favorites_sound)
                mp.start()
            }

            //VIBRATION (See Settings)
            if (viewModel.vibrationOn) {
                @Suppress("DEPRECATION")
                ContextCompat.getSystemService(context, Vibrator::class.java)?.vibrate(50)
            }

            //UPDATE
            photographs[position].favorite = !photographs[position].favorite

            holder.setFavoriteIcon(photographs[position].favorite)

            //Navigation to the same detail in order to update the favorite icon.
            if (photographs[position].id == id) {
                viewModel.comesFromDetail = true

                val action = DetailFragmentDirections.actionDetailSelf(id)
                Navigation.findNavController(holder.itemView).navigate(action)
            }
        }
    }

    /**
     * This method is used to get the total amount of items in the Recycler View.
     */
    override fun getItemCount(): Int {
        return photographs.size
    }


    //VIEW HOLDER
    class DetailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //ATTRIBUTES
        private var image: ImageView = itemView.findViewById(R.id.image)
        private var id: TextView = itemView.findViewById(R.id.photo_id)
        private var favoriteIcon: ImageView = itemView.findViewById(R.id.favorites)

        //METHODS
        /**
         * This method is used to set up the data of each item of the photographs list.
         */
        @SuppressLint("SetTextI18n")
        fun bindData(photo: Photograph, context: Context) {
            //Picasso.get().load(photo.download_url).error(R.drawable.error_loading).into(image)
            Glide.with(context).load(photo.download_url).error(R.drawable.error_loading).into(image)
            id.text = "#${photo.id}"

            setFavoriteIcon(photo.favorite)
        }

        /**
         * This method is used to set the image of the favorites icon.
         */
        fun setFavoriteIcon(isFavorite: Boolean) {
            if (isFavorite)
                favoriteIcon.setImageResource(R.drawable.favorite_icon_filled)
            else
                favoriteIcon.setImageResource(R.drawable.favorite_icon)
        }
    }
}
