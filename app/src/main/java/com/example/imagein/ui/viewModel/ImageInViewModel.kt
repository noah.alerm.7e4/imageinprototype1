package com.example.imagein.ui.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.imagein.data.models.Photograph
import com.example.imagein.data.repository.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ImageInViewModel : ViewModel() {
    //ATTRIBUTES
    private var photographs = mutableListOf<Photograph>()
    private var livePhotographs = MutableLiveData<List<Photograph>>()
    var searchedCharacters: String = ""

    //BOOLEANS
    //list
    var isFirstTime = true
    var comesFromFavorites = false
    var comesFromSearch = false
    //detail
    var comesFromDetail = false
    //settings
    var settingsEnter = true
    var darkModeOn = false
    var vibrationOn = true
    var soundOn = true

    //INIT
    init {
        //There are 34 pages of photographs on the API
        for (i in 1..34) {
            val call = Repository().getPhotographs(i)

            call.enqueue(object : Callback<List<Photograph>> {
                //RESPONSE
                override fun onResponse(call: Call<List<Photograph>>, response: Response<List<Photograph>>) {
                    if (response.isSuccessful) {
                        //The data is collected here and then added to the MutableLiveData as otherwise only one page is added to the recyclerview.
                        photographs.addAll(response.body()!!.toMutableList())
                    }
                }

                //FAILURE
                override fun onFailure(call: Call<List<Photograph>>, t: Throwable) {
                    Log.e("ERROR", t.message.toString())
                }
            })
        }

        //Collection of all pages into the MutableLiveData.
        livePhotographs.postValue(photographs)
    }

    //GETTER
    fun getPhotographs() = livePhotographs
}
