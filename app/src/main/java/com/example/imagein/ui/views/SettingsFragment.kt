package com.example.imagein.ui.views

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_NO
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.example.imagein.R
import com.example.imagein.ui.viewModel.ImageInViewModel

class SettingsFragment : PreferenceFragmentCompat() {
    //ATTRIBUTES
    private lateinit var goBackIcon: ImageView

    //view model
    private val viewModel: ImageInViewModel by activityViewModels()

    //ON VIEW CREATED
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //PREFERENCES
        val darkMode = findPreference<SwitchPreference>("dark_mode")
        val vibration = findPreference<SwitchPreference>("vibration")
        val sound = findPreference<SwitchPreference>("sound")

        //ANIMATION
        if (viewModel.settingsEnter) {
            //This is used because the switches switch themselves on start.
            darkMode!!.isChecked = viewModel.darkModeOn
            vibration!!.isChecked = viewModel.vibrationOn
            sound!!.isChecked = viewModel.soundOn
            viewModel.settingsEnter = false
            view.startAnimation(AnimationUtils.loadAnimation(requireContext(),
                R.anim.detail_enter_animation
            ))
        }

        //IDs
        goBackIcon = view.findViewById(R.id.go_back_icon)

        //ON CLICK
        goBackIcon.setOnClickListener {
            viewModel.settingsEnter = true
            findNavController().navigate(R.id.action_settings_to_list)
            view.startAnimation(AnimationUtils.loadAnimation(requireContext(),
                R.anim.detail_exit_animation
            ))
        }
    }

    //ON CREATE PREFERENCES
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        //PREFERENCES
        val darkMode = findPreference<SwitchPreference>("dark_mode")
        val vibration = findPreference<SwitchPreference>("vibration")
        val sound = findPreference<SwitchPreference>("sound")

        //ON PREFERENCE CHANGE
        //dark mode
        darkMode!!.setOnPreferenceChangeListener { _, newValue ->
            if (newValue == true) {
                AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)
                viewModel.darkModeOn = true
            }
            else {
                AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_NO)
                viewModel.darkModeOn = false
            }

            //Navigation to update settings to dark or light mode.
            findNavController().navigate(R.id.action_settings_self)

            true
        }

        //vibration
        vibration!!.setOnPreferenceChangeListener { _, newValue ->
            viewModel.vibrationOn = newValue == true

            true
        }

        //sound
        sound!!.setOnPreferenceChangeListener { _, newValue ->
            viewModel.soundOn = newValue == true

            true
        }
    }
}
